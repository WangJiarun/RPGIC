package com.rpgitem.def_item;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

/**
 * Created by winston_wang on 16/8/1.
 */
public class DefaultItem extends Item {

    String icon_string = "iron_shovel";
    IIcon icon;
    IIconRegister register;

    public DefaultItem(){
        setUnlocalizedName("default_item");
        setTextureName("iron_shovel");
        setCreativeTab(CreativeTabs.tabMisc);
    }
    @Override
    public IIcon getIcon(ItemStack stack, int renderPass, EntityPlayer player, ItemStack usingItem, int useRemaining) {
        if(stack.hasTagCompound()) {
            icon_string = stack.getTagCompound().getString("icon");
            icon = register.registerIcon(icon_string);
            return icon;
        }else{
            return null;
        }
    }
    @Override
    public void registerIcons(IIconRegister p_94581_1_) {
        register = p_94581_1_;
    }
}
