package com.rpgitem;

import com.rpgitem.Handle.power.manager.PowerManager;
import com.rpgitem.def_item.DefaultItem;
import com.rpgitem.def_item.DefaultTool;
import com.rpgitem.proxy.common.CommonProxy;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid = RPGItem.MODID, name = RPGItem.NAME,version = RPGItem.VERSION)
public class RPGItem {
    public static final String MODID = "rpgitem";
    public static final String VERSION = "1.0";
    public static final String NAME = "RPGItem";

    @SidedProxy(clientSide = "com.rpgitem.proxy.client.ClientProxy",
            serverSide = "com.rpgitem.proxy.common.CommonProxy")
    public static CommonProxy proxy;

    static public DefaultItem defi = new DefaultItem();
    static public DefaultTool deft = new DefaultTool();

    @Mod.Instance("RPGItem")
    public static RPGItem instance;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        GameRegistry.registerItem(defi,"default_item");
        GameRegistry.registerItem(deft,"default_TOOL");
        PowerManager.register();
        proxy.preInit(event);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }
}
