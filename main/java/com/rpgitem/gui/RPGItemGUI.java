package com.rpgitem.gui;

import com.rpgitem.proxy.client.ClientProxy;

import net.minecraft.client.gui.GuiScreen;

import org.lwjgl.input.Keyboard;

/**
 * Created by winston_wang on 16/8/5.
 */
public class RPGItemGUI extends GuiScreen {

    private GuiScreen parentScreen;

    public RPGItemGUI(GuiScreen parent)
    {
        parentScreen = parent;
    }

    public void initGui()
    {
        //每当界面被打开时调用
        //这里部署控件
    }

    public void drawScreen(int par1, int par2, float par3)
    {
        drawDefaultBackground();
        super.drawScreen(par1,par2,par3);
    }

    @Override
    protected void keyTyped(char p_73869_1_, int p_73869_2_) {
        super.keyTyped(p_73869_1_, p_73869_2_);
        if(mc.theWorld != null && p_73869_2_ == ClientProxy.ShowNewGui.getKeyCode())
            mc.displayGuiScreen(parentScreen);
    }
}
