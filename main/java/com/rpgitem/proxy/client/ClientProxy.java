package com.rpgitem.proxy.client;

import com.rpgitem.gui.RPGItemGUI;
import com.rpgitem.proxy.common.CommonProxy;

import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;

/**
 * Created by winston_wang on 16/7/31.
 */
public class ClientProxy extends CommonProxy {
    public static final KeyBinding ShowNewGui = new KeyBinding("rpgitem.gui.show", Keyboard.KEY_I, "rpgitem.keytitle");

    public void preInit(FMLPreInitializationEvent event) {
        //PreInit方法
    }

    public void init(FMLInitializationEvent event) {
        //Init方法 进行注册物品 方块的模型 材质
        FMLCommonHandler.instance().bus().register(this);
        ClientRegistry.registerKeyBinding(ShowNewGui);
    }

    public void postInit(FMLPostInitializationEvent event) {
        //PostInit方法
    }
    @SubscribeEvent
    public void keyListener(InputEvent.KeyInputEvent event) {
        if (ShowNewGui.isPressed())
        {
            Minecraft mc = Minecraft.getMinecraft();
            mc.displayGuiScreen(new RPGItemGUI(mc.currentScreen));
        }
    }
}
