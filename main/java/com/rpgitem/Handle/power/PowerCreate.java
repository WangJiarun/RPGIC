package com.rpgitem.Handle.power;

import com.rpgitem.Handle.RPGItemHandle;
import com.rpgitem.RPGItem;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import java.util.ArrayList;

/**
 * Created by winston_wang on 16/8/5.
 */
public class PowerCreate extends  BasePower {
    @Override
    public void start(EntityPlayer player, ArrayList<String> args) {
        RPGItemHandle.stack = new ItemStack(RPGItem.defi);
        NBTTagCompound nbt2 = new NBTTagCompound();
        nbt2.setString("Name",args.get(0));
        RPGItemHandle.stack.stackTagCompound = new NBTTagCompound();
        RPGItemHandle.stack.stackTagCompound.setTag("display",nbt2);
        RPGItemHandle.stack.stackTagCompound.setString("icon",args.get(2));
    }
}
