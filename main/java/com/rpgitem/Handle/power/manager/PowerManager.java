package com.rpgitem.Handle.power.manager;

import com.rpgitem.Handle.RPGItemHandle;
import com.rpgitem.Handle.power.BasePower;
import com.rpgitem.Handle.power.PowerCreate;
import com.rpgitem.Handle.power.PowerGive;

import java.util.ArrayList;

/**
 * Created by winston_wang on 16/8/2.
 */
public class PowerManager {

    public static ArrayList<String> register_name = new ArrayList<String>();

    public static void register(){
        PowerRegistry(PowerGive.class,"give");
        PowerRegistry(PowerCreate.class,"create");
    }
    public static void PowerRegistry(Class c,String name){
        int check = 0;
        if(register_name.size() ==0) {
            RPGItemHandle.power_class.add(c);
            RPGItemHandle.power_name.add(name);
        }else{
            for(int i = 0;i < register_name.size();i++){
                if(name != register_name.get(i)){
                    if(check == register_name.size()){
                        RPGItemHandle.power_class.add(c);
                        RPGItemHandle.power_name.add(name);
                    }
                    check++;
                }
            }
        }
    }
}
