package com.rpgitem.Handle.power;

import com.rpgitem.Handle.RPGItemHandle;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;

import java.util.ArrayList;

/**
 * Created by winston_wang on 16/8/3.
 */
public class PowerGive extends  BasePower{
    @Override
    public void start(EntityPlayer player, ArrayList<String> args) {
        player.inventory.addItemStackToInventory(RPGItemHandle.stack);
    }
}