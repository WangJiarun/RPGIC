package com.rpgitem.Handle.power;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;

import java.util.ArrayList;

/**
 * Created by winston_wang on 16/8/2.
 */
public abstract class BasePower {

    private String Name;
    private String Usage;

    public abstract void start(EntityPlayer player, ArrayList<String> args);

    public final void setName(String name){
        this.Name = name;
    }
    public final void setUsage(String usage){
        this.Usage = usage;
    }
    public String getUsage(){
        return "usage: "+ Usage;
    }
    public final String POWER_NORMAL = "true";
    public final String POWER_ERROR = "false";
}
