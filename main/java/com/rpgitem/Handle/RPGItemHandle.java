package com.rpgitem.Handle;

import com.rpgitem.Handle.power.BasePower;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;

/**
 * Created by winston_wang on 16/7/31.
 */
public class RPGItemHandle{

    public static ItemStack stack;
    public static ArrayList<String> power_name = new ArrayList<String>();
    public static ArrayList<Class<BasePower>> power_class = new ArrayList<Class<BasePower>>();

    public static void Handle(String name,ArrayList<String> al,EntityPlayer player){
        for(int i = 0;i < power_name.size();i++){
            boolean b = name.equals(power_name.get(i));
            if(b){
                try {
                    power_class.get(i).getMethod("start", EntityPlayer.class, ArrayList.class).invoke(power_class.get(i).newInstance(),player,al);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
}